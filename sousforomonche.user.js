// ==UserScript==
// @name         New Userscript
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://onche.org/*
// @icon         data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

	// Find the parent block with class name 'bloc border blue'
var parentBlock = document.querySelector('.bloc.border.blue');

// Check if the parent block is found
if (parentBlock) {
    var linkElement = document.createElement('a');
    linkElement.href = 'https://codeberg.org/Annapurna/sousforomonche/raw/branch/main/linktoforum.png';
    linkElement.textContent = 'Forum des anciens';
    
    var linkElementbis = document.createElement('a');
    linkElementbis.href = 'https://codeberg.org/Annapurna/sousforomonche/raw/branch/main/linktoforum.png';
    linkElementbis.textContent = 'Forum des admins';
    
    var linkElementtris = document.createElement('a');
    linkElementtris.href = 'https://codeberg.org/Annapurna/sousforomonche/raw/branch/main/linktoforum.png';
    linkElementtris.textContent = 'Forum des triples diamants';
    
    
    
    var contentLinksBlock = parentBlock.querySelector('.content.links');
    if (contentLinksBlock) {
        contentLinksBlock.appendChild(linkElement);
        contentLinksBlock.appendChild(linkElementbis);
        contentLinksBlock.appendChild(linkElementtris);
    } else {
        console.log('Content links block not found.');
    }
} else {
    console.log('Parent block not found.');
}

})();
